import React, { Component, useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Button, Text,View } from 'native-base';
import {StyleSheet, Image,Animated,Easing,Alert}  from 'react-native';

import { TextInput } from 'react-native-gesture-handler';
import { Container, Header, Content, Form, Item, Input } from 'native-base';
import {   Card, CardItem, Thumbnail,   Icon, Left, Body } from 'native-base';




const HomeScreen  = () => {
    return (

      <Container>
        <Header><Text style={{marginTop:20,fontWeight:'bold'}}>Header</Text></Header>
        <Content>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'https://i.insider.com/5d71401f6f24eb04b07dbdf3?width=1100&format=jpeg&auto=webp'}} />
                <Body>
                  <Text>Ouhsa</Text>
                  <Text note>April 28, 2020</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Image  source={{uri: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/selena-gomez-vote-2020-1603480143.jpg?crop=0.600xw:0.600xh;0.218xw,0.107xh&resize=640:*'}} style={{height: 200, width: 200, flex: 1}}/>
                <Text>
                Selena Marie Gomez is an American singer, actress, and producer. Born and raised in Texas, Gomez began her career by appearing on the children's television series Barney & Friends. Wikipedia
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                 
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>
        </Content>
      </Container>


     
      
    );
  }
 
  setTimer = () => {
    Alert.prompt(
      'Set Timer',
      null,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: (time) => this.startFan(time),
        },
      ],
      'plain-text',
    );
  };


  




  
  const Task = () => {

//     const [todo, setTodo] = useState([{}])


//     const deleteHandler = (key) => {
//         setTodo((prevTodo) => {
//             return prevTodo.filter(todo => todo.key != key)
//         })
//     }

//     const addHandler = (title, description) => {
//         setTodo((prevTodo) => {
//             return [
//                 { title: title, description: description, key: Math.random().toString() },
//                 ...prevTodo
//             ]
//         })
//     }

//     const { container } = style
//     return (
//         <View style={container}>
//             <AddTodo addHandler={addHandler} />
//             <View>
//                 <FlatList
//                     data={todo}
//                     renderItem={({ item }) => (
//                         <TodoItem item={item} deleteHandler={deleteHandler} />
//                     )
//                     }
//                 />
//             </View>

//         </View>
//     )
// }

// const AddTodo = ({item, addHandler}) => {
    
//   const [title, setTitle] = useState('')
//   const [description, setDescription] = useState('')
  
//   const titleChangeHandler = (val) =>{
//       setTitle(val)
//   }

//   const descChangeHandler = (val) => {
//       setDescription(val)
//   }
//   const { headingStyle } = styles
    return (

            <Container>
                  <Header><Text style={{marginTop:20,fontWeight:'bold'}}>Headers</Text></Header>
                    <Text style={{fontWeight:'bold',fontSize:20,marginLeft:10}}>Add Task</Text>
                  <Content>
                    <Form style={{marginTop:10}}>
                      <Item>
                        <Input placeholder="Title"/>
                      </Item>
                      <Item last>
                        <Input placeholder="Description" />
                      </Item>
                      <Button style={{marginTop:10,width:400,marginLeft:5}}  primary><Text style={{marginLeft:165}}>Save</Text></Button>
                    </Form>
                  </Content>
            </Container>
  
      
    );
  }

 

  const Fan = () =>{

    spinValue = new Animated.Value(0);

  // First set up animation 
  const startAnimation = () => {
      Animated.loop(
          Animated.timing(
              this.spinValue,
              {
                  toValue: 10,
                  duration: 400,
                  easing: Easing.linear,
                  useNativeDriver: true
              }
          )
      ).start()
  }

  const stopAnimation = () => {
      Animated.timing(
          this.spinValue,
          {
              toValue: 0,
              //  easing: Easing.linear,
              useNativeDriver: true
          }
      ).stop()

  }

  // Next, interpolate beginning and end values (in this case 0 and 1)
  const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
  })
  const {container, buttonContainer} = styles
        
        return(
          <Container>
          <Header><Text style={{marginTop:20,fontWeight:'bold'}}>Header</Text></Header>
          <Content>
            <Card style={{flex: 0}}>
            
              <CardItem>
                <Body>
                  <Animated.Image
                   style={{ width: 220, height: 220,flex: 1,marginTop:23,marginLeft:80, transform: [{ rotate: spin }] }}
                      source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNQv2bT22vDxZq9kM_MmT-3152GCnj11At6w&usqp=CAU'}} />
                  <Image source={{uri:'https://icon-library.com/images/circle-icon/circle-icon-17.jpg'}} style={{height: 275, width: 275, flex: 1,marginLeft:55,position:'absolute'}}/>

                  <Button style={{marginTop:200,marginLeft:50,borderRadius:25}} onPress={startAnimation}><Text>On</Text></Button>
                  <Button  danger style={{marginLeft:130,marginTop:-45,borderRadius:25}} onPress={stopAnimation}><Text>Off</Text></Button>
                  
                  <Button dark style={{marginLeft:210,marginTop:-45,borderRadius:25}} onPress={setTimer} ><Text>Set Time</Text></Button>
                </Body>
              </CardItem>
             
            </Card>
          </Content>
        </Container>

        );
  }

  const Tab = createBottomTabNavigator();

 
 
 

 class Homepage extends Component {
   
    render() {
        return (
        
          
            
            <NavigationContainer>
             
                <Tab.Navigator >
                    <Tab.Screen  name="Home" component={HomeScreen} />
                    <Tab.Screen name="Task" component={Task} />
                    <Tab.Screen name="Fan" component={Fan}/>
                    
                </Tab.Navigator>
             </NavigationContainer>
           

         
        )
    }
}

const styles = StyleSheet.create({
  
  
  logofb: {
    width: 70,
    height: 70,
    backgroundColor: 'powderblue',
    margin: 5,
    borderRadius: 50,
   marginTop:50,
    marginRight:300

  },
  textcolo:{
    color:'black',
    fontWeight:'bold'
  },
  home:{
    fontWeight:'bold',
    
  },
  tasktex:{
    marginBottom:500,
    marginRight:260,
    fontWeight:'bold',
    fontSize:23
  }
  
});
export default Homepage;




